<?php

class mongodb_views_handler_filter_numeric extends views_handler_filter_numeric {
  function operators() {
    $original_operators = parent::operators();
    $operator_map = array(
      '<' => '$lt',
      '<=' => '$lte',
      '>' => '$gt',
      '>=' => '$gte',
      '!=' => '$ne',
      'between' => 'between',
    );
    $operators = array();
    foreach ($operator_map as $old => $new) {
      $operators[$new] = $original_operators[$old];
    }
    return $operators;
  }

  function op_simple($field) {
    // Hack: we know it starts with a dot.
    $field = substr($field, 1);
    $value = $this->value['value'];
    $this->query->mongo->query = array($field => array($this->operator => (int)$value));
  }
  
  function op_between($field) {
    // Hack: we know it starts with a dot.
    $field = substr($field, 1);
    $this->query->mongo->query = array($field => array('$gte' => (int)$this->value['min'], '$lte' => (int)$this->value['max']));
  }
}