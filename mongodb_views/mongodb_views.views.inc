<?php

// Implementation of hook_views_data
function mongodb_views_views_data() {
  $data['mongo']['table']['group']  = 'MongoDB';
  $data['mongo']['table']['base'] = array(
    'title' => 'mongo',
    'help' => t('Results from MongoDB'),
    'query class' => 'mongoViewsQuery'
  );
  for ($i = 0; $i < 6; $i++) {
    $attr = "attribute$i";
    $data['mongo'][$attr] = array(
      'title' => $attr,
      'help' => $attr,
      'field' => array(
        'click sortable' => TRUE,
        'handler' => 'views_handler_field_numeric',
      ),
    );
    $data['mongo'][$attr]['filter']['handler'] = 'mongodb_views_handler_filter_numeric';

  }
  return $data;
}
