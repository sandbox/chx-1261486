<?php

class mongoViewsQuery extends views_plugin_query {

  var $args = array();
  var $method = '';

  function init($table, $field) {
    $connection = new Mongo;
    // @TODO: make this more generic.
    $this->mongo->collection= $connection->selectDB('dbname')->selectCollection('views');
    $this->mongo->query = array();
  }

  function add_field($table, $field, $alias = '', $params = NULL) {
    return $field;
  }

  function add_orderby($table, $field, $order, $alias = '') {
    $this->order[] = array($field => strtoupper($order) == 'ASC' ? 1 : -1);
  }

  function ensure_table() {}

  /**
   * Execute a call to mongo.
   */
  function execute(&$view) {

    // What page was requested:
    $pager_page_array = isset($_GET['page']) ? explode(',', $_GET['page']) : array();
    if (!empty($pager_page_array[$view->pager['element']])) {
      $page = intval($pager_page_array[$view->pager['element']]);
    }
    $per_page = empty($view->pager['items_per_page']) ? 500 : $view->pager['items_per_page'];
    $cursor = $this->mongo->collection->find($this->mongo->query);
    $total_rows = $cursor->count();
    $cursor = $cursor->limit($per_page)->skip($page * $per_page);
    foreach ($cursor as $result) {
      $view->result[] = (object)$result;
    }

    if (!empty($view->pager['items_per_page'])) {
      // We no longer use pager_query() here because pager_query() does not
      // support an offset. This is fine as we don't actually need pager
      // query; we've already been doing most of what it does, and we
      // just need to do a little more playing with globals.
      if (!empty($view->pager['use_pager']) || !empty($view->get_total_rows)) {
        // We use total because that's the number that actually can return
        // as capped by max_matches. total_found is the number of real results.
        $view->total_rows = $total_rows;
      }

      if (!empty($view->pager['use_pager'])) {
        // dump information about what we already know into the globals
        global $pager_page_array, $pager_total, $pager_total_items;
        // total rows in query
        $pager_total_items[$view->pager['element']] = $view->total_rows;
        // total pages
        $pager_total[$view->pager['element']] = intval($view->total_rows / $per_page) + 1;

        // If the requested page was within range. $view->pager['current_page']
        // defaults to 0 so we don't need to set it in an out-of-range condition.
        if (isset($page)) {
          if ($page > 0 && $page < $pager_total[$view->pager['element']]) {
            $view->pager['current_page'] = $page;
          }
        }
        $pager_page_array[$view->pager['element']] = $view->pager['current_page'];
      }
    }
  }
}