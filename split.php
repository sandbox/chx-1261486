<?php
// $Id: split.php,v 1.8 2005/10/21 09:24:35 killes Exp $
include_once('./includes/bootstrap.inc');
include_once('./includes/file.inc');
include_once('./modules/system.module');

// simplified version of check_directory, no message and so on
function check_directory($directory, $mode = 0, $form_item = NULL) {
  chmod($directory, 0777);
  if (!is_dir($directory)) {
    if (($mode & FILE_CREATE_DIRECTORY) && @mkdir($directory, 0760)) {
    }
    else {
      return false;
    }
  }
  if (!is_writable($directory)) {
    if (($mode & FILE_MODIFY_PERMISSIONS) && @chmod($directory, 0760)) {
    }
    else {
      return false;
    }
  }
  return true;
}

if(!function_exists('file_put_contents')) {
  function file_put_contents($filename, $data, $file_append = false) {
   chmod($filename, 0644);
   $fp = fopen($filename, (!$file_append ? 'w+' : 'a+'));
   if(!$fp) {
     trigger_error('file_put_contents cannot write in file.', E_USER_ERROR);
     return;
   }
   fputs($fp, $data);
   fclose($fp);
  }
}
$dir = 'files/split';
if (!check_directory($dir, 1)) {
  die('Cannot write split directory');
}
$files = array_values(system_listing('\.module$', 'modules', 'name', 0));
$files = array_merge($files, system_listing('\.inc$', 'includes', 'name', 0));
$exceptions = array(
	'check_plain', 
	'_taxonomy_get_tid_from_term', 
	'_search_keywords_truncate', 
	'_search_excerpt_replace', 
	'_fix_gpc_magic', 
	'_menu_sort', 
	'_user_sort', 
	'user_access', 
	'valid_input_data', 
	'locale_initialize', 
	'fix_gpc_magic', 
	'menu_execute_active_handler', 
	'drupal_not_found', 
	'drupal_access_denied', 'drupal_page_footer', 
	'error_handler', 
	'element_child', '_drupal_bootstrap_full',
	'_form_sort');
$included = array('bootstrap', 'session', 'database', 'module');
foreach ($files as $key => $file) {
  if (in_array($file->name, $included)) {
    unset($files[$key]);
    continue;
  }
  $buf = file_get_contents($file->filename);
  $tokens = token_get_all($buf);
  $n = count($tokens);
  for ($i = 1; $i < $n; $i++) {
    $token = $tokens[$i];
    if (!is_string($token)) {
    	$t = $token;
      list ($id, $token) = $t;
      if ($id == T_FUNCTION) {
        // tokens[$i + 1] is a space
        $fn = $tokens[$i + 2][1];
        // is the first parameter a reference?
        if ($tokens[$i + 4] == '&') {
          $references[$fn] = 1;
        }
        $functions[$fn] = 1;
        // the loop is inside a function
        $in_fn = TRUE;
        $ref_counter = 0;
      }
    }
    elseif ($in_fn) {
      // is this a reference?
      if ($token == '&' && !is_string($tokens[$i + 1]) && $tokens[$i + 1][0] == T_VARIABLE) {
        $ref_counter++;
      }
      if ($token == ';') {
        $in_fn = FALSE;
        // references collect the functions where the first and only the first
        // parameter is a reference
        if ($ref_counter>1 || ($ref_counter == 1 && !isset($references[$fn]))) {
          unset($functions[$fn], $references[$fn]);
          $exceptions[] = $fn;
        }
      }
    } // else if
  } // for
} // foreach
foreach ($files as $file) {
  $module = '';
  $buf = file_get_contents($file->filename);
  $tokens = token_get_all($buf);
  $n = count($tokens);
  $out = &$module;
  $func = '';
  $depth = 0;
  for ($i = 0; $i < $n; $i++) {
    $token = $tokens[$i];
    if (!is_string($token)) {
      $t = $token;
      list ($id, $token) = $t;
      if ($id == T_COMMENT) continue;
      if ($id == T_FUNCTION) {
        $out = &$func;
      }
      // A string followed by a ( is either a function call or a definition
      // but we do not want definitons
      if ($id == T_STRING && $tokens[$i + 1] == '(' && (is_string($tokens[$i - 2]) || $tokens[$i - 2][0] != T_FUNCTION)) {
        if (isset($functions[$token])) {
          // if we are inside a function call, and see another function call,
          // then we need a comma. For eg: c('l', c('t', 'title'), 'link)
          $out .= ($in_c ? ', ' : '') .'c'. (isset($references[$token]) ? '1' : '') ."('". $token ."'";
          $in_c = TRUE;
          $i++;
          continue;
        }
        elseif ($token == 'function_exists') {
          $token = 'drupal_function_exists';
        }
      }
      if ($id == T_FUNCTION) {
        $filename = $tokens[$i+2][1];
      }
    }
    if ($in_c) {
      $in_c = FALSE;
      if ($token != ')') {
        $token = ', '. $token;
      }
    }
    if ($token == '{') {
      $depth++;
    }
    if ($token == '}') {
      $depth--;
      if (!$depth) {
      	$current = "$dir/". $filename{0} . $filename{1};
        check_directory($current, 1);
        $map[$file->name][] = "$filename";
        file_put_contents("$current/$filename.inc", "<?php " . $out .'}');
        $func = '';
        $out = &$module;
        continue;
      }
    }
    $out .= $token;
  }
  file_put_contents("$dir/$file->basename", $module);
}
$files = system_listing('\.engine$', 'themes', 'name', 0);
$files = array_merge($files, system_listing('\.php$', 'themes', 'name', 0));
foreach ($files as $file) {
  $buf = file_get_contents($file->filename);
  $tokens = token_get_all($buf);
  $n = count($tokens);
  for ($i = 0; $i < $n; $i++) {
    if (!is_string($tokens[$i]) && isset($functions[$tokens[$i][1]]) && !in_array($tokens[$i][1], $exceptions) && $tokens[$i][1] != 'module_exist') {
      $exceptions[] = $tokens[$i][1];
    }
  }
}
$output = '<?php $exceptions = '. var_export($exceptions, true);
//$output .= ' ;$map = '. var_export($map, TRUE);
file_put_contents($dir .'/'. '__exceptions.inc',  $output . "\n?>");
echo 'done';
?>
