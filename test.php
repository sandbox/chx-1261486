<?php

//$_GET['q'] = 'node/12/edit/foo';
$_GET['q'] = 'admin/user/rules/delete';
// we limit the number of parts a menu can define so we do not have a DoS
$parts = explode('/', $_GET['q'], 6);
$n1 = count($parts);
$placeholders = $possible_paths = array();
$end = (1 << $n1) - 1;
$length = $n1 - 1;
// we will use binary numbers where bit 1 represents original value and
// bit 0 means wildcard. If the path is node/12/edit/foo then the 110
// bitstring represents node/%/edit where % means that any argument
// matches that part
for ($i = $end; $i > 0; $i--) {
  $current = '';
  $count = 0;
  for ($j = $length; $j >= 0; $j--) {
    if ($i & (1 << $j)) {
      $count++;
      $current .= $parts[$length - $j];
    }
    else {
      $current .= '%';
    }
    if ($j) {
      $current .= '/';
    }
  }
  // if the number was like 10...0 then the next number will be 11...11
  // one bit less wide
  if ($count == 1) {
    $length--;
  }
  $placeholders[] = "'%s'";
  $possible_paths[] = $current;
}
function menu_unserialize($data) {
  if ($data) {
    $data = unserialize($data);
    foreach ($data as $k => $v) {
      if (is_int($v)) {
        $data[$k] = arg($v);
      }
    }
    return $data;
  }
  else {
    return array();
  }
}
require_once('./includes/bootstrap.inc');
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
$item = db_fetch_object(db_query_range('SELECT * FROM {menu_new} WHERE path IN ('. implode (',', $placeholders) .') ORDER BY db_weight DESC', $possible_paths, 0, 1));
  // TODO: user_logged_in() {return $GLOBALS['user']->uid;} menu_false() {return FALSE;} menu_true() {return TRUE;}
/*
echo "<pre>access callback: $item->access, arguments: ". var_export(menu_unserialize($item->access_arguments), TRUE);
$args = array_merge(menu_unserialize($item->callback_arguments), array_slice($parts, $item->parts));
echo "\nmenu callback: $item->callback, arguments: ". var_export($args, TRUE);
echo "\nbreadcrumb: ". implode('&gt;', unserialize($item->breadcrumb));
die();
if ((is_numeric($item->access) && $item->access) || call_user_func_array($item->access, menu_unserialize($item->access_arguments))) {
  call_user_func_array($item->callback, $args);
}
else {
  drupal_access_denied();
}
*/
// navigation
$result = db_query('SELECT mid, path, title FROM {menu_new} WHERE pid IN (%s) ORDER BY vancode', $item->parents);
while ($item = db_fetch_object($result)) {
  echo "$item->mid $item->path<br>";
}