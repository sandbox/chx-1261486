<?php
// $Id: query.inc,v 1.1 2010/04/17 22:13:01 chx Exp $

/**
 * @ingroup database
 * @{
 */

/**
 * @file
 * Query code for MongoDB, falling back to MySQL embedded database engine.
 */

include_once DRUPAL_ROOT . '/includes/database/mysql/query.inc';

function _mongodb_query_translate_conditions($conditions) {
  $find = array();
  $map = array(
    '<>' => '$ne',
    'IN' => '$in',
    'NOT IN' => '$nin',
  );
  $conjunction = $conditions['#conjunction'];
  unset($conditions['#conjunction']);
  foreach ($conditions as $condition) {
    $operator = $condition['operator'];
    if ($operator == '=') {
      $find[$condition['field']] = $condition['value'];
    }
    elseif (isset($map[$operator])) {
      $find[$condition['field']] = array($map[$operator] => $condition['value']);
    }
    else {
      throw new Exception("Don't know how to handle " . var_export($condition, TRUE));
    }
  }
  return $find;
}

function _mongodb_query_tables() {
  return array('system', 'menu_router', 'registry');
}

class InsertQuery_mongo extends InsertQuery_mysql {
  function execute() {
    if (in_array($this->table, _mongodb_query_tables())) {
      $collection = mongodb_collection($this->table);
      foreach ($this->insertValues as $insert_values) {
        $save = array();
        foreach ($insert_values as $key => $value) {
          $save[$this->insertFields[$key]] = $value;
        }
        $collection->insert($save);
      }
    }
    return parent::execute();
  }
}

class UpdateQuery_mongo extends UpdateQuery {
  function execute() {
    if (in_array($this->table, _mongodb_query_tables())) {
      $find = _mongodb_query_translate_conditions($this->condition->conditions());
      mongodb_collection($this->table)->update($find, array('$set' => $this->fields), array('multiple' => TRUE));
    }
    return parent::execute();
  }
}

class MergeQuery_mongo extends MergeQuery_mysql {
}

class DeleteQuery_mongo extends DeleteQuery {
  function execute() {
    if (in_array($this->table, _mongodb_query_tables())) {
      $find = _mongodb_query_translate_conditions($this->condition->conditions());
      mongodb_collection($this->table)->remove($find);
    }
    return parent::execute();
  }
}

/**
 * @} End of "ingroup database".
 */
