<?php
// $Id: schema.inc,v 1.1 2010/04/17 22:13:01 chx Exp $

/**
 * @file
 * Database schema code for MySQL database servers.
 */

include_once DRUPAL_ROOT . '/includes/database/mysql/schema.inc';

/**
 * @ingroup schemaapi
 * @{
 */

class DatabaseSchema_mongo extends DatabaseSchema_mysql {
}

/**
 * @} End of "ingroup schemaapi".
 */
