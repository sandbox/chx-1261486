<?php
// $Id: install.inc,v 1.1 2010/04/17 22:13:01 chx Exp $

/**
 * @file
 * Installation code for MongoDB.
 */


// MongoDB specific install functions

class DatabaseTasks_mongo extends DatabaseTasks {
  protected $pdoDriver = 'mysql';
  public function name() {
    return 'Mongo';
  }
}
