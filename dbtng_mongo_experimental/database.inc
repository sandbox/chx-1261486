<?php
// $Id: database.inc,v 1.1 2010/04/17 22:13:01 chx Exp $

/**
 * @file
 * Database interface code for MongoDB, falling back to MySQL database servers.
 */

include_once DRUPAL_ROOT . '/includes/database/mysql/database.inc';
include_once DRUPAL_ROOT . '/sites/all/modules/mongodb/mongodb.module';

/**
 * @ingroup database
 * @{
 */

class DatabaseConnection_mongo extends DatabaseConnection_mysql {
  function driver() {
    return 'mongo';
  }
  function query($query, array $args = array(), $options = array()) {
    static $seen = array();
    if (!isset($seen[$query])) {
      $seen[$query] = $query;
      preg_match('/\{(.*)\}/', $query, $matches);
      print substr($query, 0, 200) ."<br>";
    }
    return parent::query($query, $args, $options);
  }
  function queryRange($query, $from, $count, array $args = array(), array $options = array()) {
    preg_match('/\{(.*)\}/', $query, $matches);
    print substr($query, 0, 80) ."<br>";
    return parent::query($query, $args, $options);
  }
}

/**
 * @} End of "ingroup database".
 */
