<?php
// $Id: session-memcache.inc,v 1.2 2006/12/12 12:58:53 chx Exp $

/**
 * @file
 * User session handling functions.
 */

function sess_open($save_path, $session_name) {
  global $session_memcached;
  $session_memcached = NULL;
  foreach (variable_get('session_memcached', array()) as $server_conf) {
    if (!isset($server_conf[1])) {
      $server_conf[1] = '11211';
    }
    if (isset($session_memcached)) {
      memcache_add_server($session_memcached, $server_conf[0], $server_conf[1]);
    }
    else {
      $session_memcached = memcache_connect($server_conf[0], $server_conf[1])
    }
  }
  return TRUE;
}

function sess_close() {
  return TRUE;
}

function sess_read($key) {
  global $user, $session_memcached;

  // Write and Close handlers are called after destructing objects since PHP 5.0.5
  // Thus destructors can use sessions but session handler can't use objects.
  // So we are moving session closure before destructing objects.
  register_shutdown_function('session_write_close');

  // Handle the case of first time visitors and clients that don't store cookies (eg. web crawlers).
  if (!isset($_COOKIE[session_name()])) {
    $user = drupal_anonymous_user();
    return '';
  }

  // Otherwise, if the session is still active, we have a record of the client's session in the database.
  $user = memcache_get($key);

  if (!$user || !$user->uid) {
    $session = isset($user->session) ? $user->session : '';
    $user = drupal_anonymous_user($session);
  }

  return $user->session;
}

function sess_write($key, $value) {
  global $user, $session_memcached;

  // If the client doesn't have a session, and one isn't being created ($value), do nothing.
  if (empty($_COOKIE[session_name()]) && empty($value)) {
    return TRUE;
  }

  $result = memcache_get($key);
  // Only save session data when when there is a session already, the user is 
  // logged in, the browser sends a cookie. This keeps crawlers out of session 
  // table. This improves speed up queries, reduces memory, and gives more 
  // useful statistics. We can't eliminate anonymous session table rows without 
  // breaking throttle module and "Who's Online" block.
  if ($result || $user->uid || $value || count($_COOKIE)) {
    // 'unload' user to make it smaller
    $fields = array_flip(user_fields());
    $fields['roles'] = 1;
    foreach ((array)$user as $key => $v) {
      if (!isset($fields[$key])) {
        unset($user->$key);
      }
    }
    $user->session = $value;
    $user->timestamp = time();
    memcache_set($session_memcached, $key, $user, FALSE, ini_get("session.gc_maxlifetime"));
  }
  // remove this part if you want even more speed and do not care about when the
  // user last visited
  if ($result && $user->uid) {
    // TODO: this can be an expensive query. Perhaps only execute it every x minutes. Requires investigation into cache expiration.
    db_query("UPDATE {users} SET access = %d WHERE uid = %d", time(), $user->uid);
  }

  return TRUE;
}

/**
 * Called when an anonymous user becomes authenticated or vice-versa.
 */
function sess_regenerate() {
  global $session_memcached;
  $old_session_id = session_id();

  // We code around http://bugs.php.net/bug.php?id=32802 by destroying
  // the session cookie by setting expiration in the past (a negative
  // value).  This issue only arises in PHP versions before 4.4.0,
  // regardless of the Drupal configuration.
  // TODO: remove this when we require at least PHP 4.4.0
  if (isset($_COOKIE[session_name()])) {
    setcookie(session_name(), '', time() - 42000, '/');
  }

  session_regenerate_id();
  $object = memcache_get($session_memcached, $old_session_id);
  memcache_delete($session_memcached, $old_session_id);
  memcache_set($session_memcached, session_id(), $object, FALSE, ini_get("session.gc_maxlifetime"));
}

/**
 * Counts how many users have sessions. Can count either anonymous sessions, authenticated sessions, or both.
 * Would be insane slow with memcached as we would need to retrieve at least the stats of all object. 
 * Not implemented.
 */
function sess_count($timestamp = 0, $anonymous = true) {
}

/**
 * Called by PHP session handling with the PHP session ID to end a user's session.
 *
 * @param  string $sid
 *   the session id
 */
function sess_destroy_sid($sid) {
  global $session_memcached;
  memcache_delete($session_memcached, $sid);
}

/**
 * End a specific user's session. Not with memcached.
 */
function sess_destroy_uid($uid) {
}

function sess_gc($lifetime) {
  // Automatic with memcached.
  // Be sure to adjust 'php_value session.gc_maxlifetime' to a large enough
  // value. For example, if you want user sessions to stay in your database
  // for three weeks before deleting them, you need to set gc_maxlifetime
  // to '1814400'. At that value, only after a user doesn't log in after
  // three weeks (1814400 seconds) will his/her session be removed.
  return TRUE;
}

if (!function_defnied('drupal_anonymous_user')) {
  function drupal_anonymous_user($session = '') {
    $user = new stdClass();
    $user->uid = 0;
    $user->hostname = $_SERVER['REMOTE_ADDR'];
    $user->roles = array();
    $user->roles[DRUPAL_ANONYMOUS_RID] = 'anonymous user';
    $user->session = $session;
    return $user;
  }
}